﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Inf_System.Models
{
    public class TestViewModel
    {
        //[Required]
        [Display(Name = "Field1")]
        public string Field1 { get; set; }

        //[Required]
        [Display(Name = "Field2")]
        public string Field2 { get; set; }

        //[Required]
        [Display(Name = "Field3")]
        public int Field3 { get; set; }
    }

    public class NewTestViewModel
    {
        //[Required]
        [Display(Name = "Field")]
        public int Field { get; set; }
    }
}