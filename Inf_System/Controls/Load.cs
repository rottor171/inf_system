﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Inf_System.Models;
using System.Threading.Tasks;

namespace Inf_System
{
    public partial class Load
    {
        public struct WSInfo
        {
            public string name;
            public string txt;
        }

        public class ClientData
        {
            public int uId = -1;
            public bool cond = false;
            public bool log_proced = false;
            public bool addingsite = false;
        }

        public static Dictionary<string, ClientData> clients = new Dictionary<string, ClientData>();

        public static string Id()
        {
            return System.Web.HttpContext.Current.User.Identity.GetUserId();
        }

        public static int uId()
        {
            return clients.FirstOrDefault(val => val.Key == Id()).Value.uId;
        }

        public static string LoadList()//Загрузка списка сайтов
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == uId()) == null)
            {
                return "nothing";
            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == uId());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> list3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    var lll = context.USLink.FirstOrDefault(sid => sid.Sid == ll.Id && sid.Id == elem.Sid);
                    if (ll != null && lll != null)
                    {
                        if (lll.Seen == true) list2.Add(ll);
                        else list3.Add(ll);
                    }
                }
                foreach (WebSite elem in list3)
                {
                    list.Add("* " + elem.Link);
                }
                foreach (WebSite elem in list2)
                {
                    list.Add(elem.Link);
                }
                var json = JsonConvert.SerializeObject(list);
                return json;
                //Program.main.WSList_Load(list);
            }
        }

        public static string LoadNotifList()//Загрузка списка уведомлений
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(uid => uid.Id == uId()) == null)
            {
                return "nothing";
            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.Notification.Where(uid => uid.Uid == uId() && (DateTime.Now.Date - uid.PrevDate.Date).TotalDays <= uid.Time);
                List<Notification> l2 = l1.ToList();
                foreach (Notification elem in l2)
                {
                    list.Add(elem.Name);
                    elem.PrevDate = DateTime.Now.AddDays(elem.Time);
                    context.Notification.Update(elem);
                    context.SaveChanges();
                }
                var json = JsonConvert.SerializeObject(list);
                return json;
                //Program.main.NotList_Load(list);
            }

        }

        public static string Update_L(string lnk, int t, string name, string txt)//Добавление нового сайта в список
        {
            Notif_BaseContext context = new Notif_BaseContext();
            var webs = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            var ws = context.WebSite.LastOrDefault();
            int newid = 0;
            if (ws != null) newid = ws.Id + 1;
            var newsite = new WebSite
            {
                Id = newid,
                Link = lnk,
                Txt = txt,
                Name = name,
                PrevDate = DateTime.Now
            };
            var x = context.WebSite.FirstOrDefault(sid => sid.Link == lnk);
            if (context.WebSite.Count(sid => sid.Link == lnk) == 0) context.WebSite.Add(newsite);//если сайт с такой ссылкой не существует, добавляем новый
            else if (x != null) { newsite.Id = x.Id; }
            var tusl = context.USLink.FirstOrDefault(sid => sid.Uid == uId() && sid.Sid == newsite.Id);
            if (tusl == null)
            {
                var ls = context.USLink?.Max(sid => sid.Id);
                int newidd = 0;
                if (ls != null) newidd = ls.Value + 1;
                var newlink = new USLink
                {
                    Id = newidd,
                    Uid = uId(),
                    Sid = newsite.Id,
                    Time = t,
                    Seen = false,
                    LastDate = DateTime.Now
                };
                context.USLink.Add(newlink);
                context.SaveChanges();
            }
            return LoadList();
        }

        public static string Update_N(string n, int t, string text, DateTime date)//Добавление нового уведомления в список
        {
            Notif_BaseContext context = new Notif_BaseContext();
            var ws = context.Notification?.Max(sid => sid.Id);
            int newid = 0;
            if (ws != null) newid = ws.Value + 1;
            var newnote = new Notification
            {
                Id = newid,
                Name = n,
                Txt = text,
                PrevDate = date,
                Time = t,
                Uid = uId()

            };
            context.Add(newnote);
            context.SaveChanges();
            return LoadNotifList();
        }

        public static string LoadPrev(string link)//загрузка превью сайта
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.WebSite.FirstOrDefault(sid => sid.Link == link) == null) { return "nothing"; }
            else
            {
                var us = context.WebSite.First(sid => sid.Link == link);
                var ls = context.USLink.First(sid => sid.Sid == us.Id);
                ls.Seen = true;
                context.USLink.Update(ls);
                context.SaveChanges();
                List<Object> li = new List<object>() { us.Link, us.Name, us.PrevDate, ls.Time };
                var json = JsonConvert.SerializeObject(li);
                return json;
                //Program.prev.LoadPrevPanel(us.Link, us.Name, us.PrevDate, ls.Time);
            }
        }

        public static string DelSite(string lnk)//удаление сайта
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.WebSite.FirstOrDefault(sid => sid.Link == lnk) == null) { return "nothing"; }
            else
            {
                var s = context.WebSite.First(sid => sid.Link == lnk);
                var us = context.USLink.First(sid => sid.Sid == s.Id && sid.Uid == uId());
                context.USLink.Remove(us);
                context.SaveChanges();
                return LoadList();
            }
        }

        public static string DelNot(string txt)//удаление уведомления
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(sid => sid.Name == txt) == null) { return "nothing"; }
            else
            {
                var us = context.Notification.First(sid => sid.Name == txt);
                context.Remove(us);
                context.SaveChanges();
                return LoadNotifList();
            }
        }

        public static string LoadNotPrev(string n)//загрузка превью уведомления
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.Notification.FirstOrDefault(sid => sid.Name == n) == null) { return "nothing"; }
            else
            {
                var us = context.Notification.First(sid => sid.Name == n);
                //Program.notprev.LoadNotifPanel(us.Name, us.Txt, us.PrevDate, us.Time);
                List<Object> li = new List<object>() { us.Name, us.Txt, us.PrevDate, us.Time };
                var json = JsonConvert.SerializeObject(li);
                return json;
            }
        }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string RefreshT(int tim)//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Id == uId()) == null)
            {
                return "nothing";
            }
            else
            {
                List<String> list = new List<String>();
                var l1 = context.USLink.Where(uid => uid.Uid == uId());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid && elem.Uid == uId());
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Time == tim);
                    if (ll == null) { }
                    else
                    {
                        if ((item.PrevDate - ll.LastDate).TotalMinutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt == ii.txt) { }
                            else
                            {

                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                return LoadList();
            }
        }

        public static string Refresh()//Обновление списка
        {
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.USLink.FirstOrDefault(uid => uid.Uid == uId()) == null) { return "nothing"; }
            else
            {
                var l1 = context.USLink.Where(uid => uid.Uid == uId());
                List<USLink> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (USLink elem in l2)
                {
                    var ll = context.WebSite.FirstOrDefault(sid => sid.Id == elem.Sid);
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = context.USLink.FirstOrDefault(sid => sid.Sid == item.Id && sid.Uid == uId());
                    if (ll == null) { }
                    else
                    {
                        if (DateTime.Now.Subtract(item.PrevDate).TotalMinutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt != ii.txt)
                            {
                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                context.WebSite.Update(item);
                                context.USLink.Update(ll);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                return LoadList();
            }
        }
        public static bool LogIn(ApplicationUser user, string password)//Логин
        {
            var login = user.Email;
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.User.FirstOrDefault(uid => uid.Login == login && uid.Password == password) == null)
            {
                if (context.User.FirstOrDefault(uid => uid.Login == login) != null || context.User.FirstOrDefault(uid => uid.Password == password) != null)
                    return false;
            }
            var i = context.User.FirstOrDefault(uid => uid.Login == login && uid.Password == password);
            clients.Add(Id(),new ClientData() { uId = i.Id});
            return true;

        }
        public static bool RegUs(ApplicationUser user, string password)//регистрация пользователя
        {
            var login = user.Email;
            Notif_BaseContext context = new Notif_BaseContext();
            if (context.User.FirstOrDefault(uid => uid.Login == login) != null) return false;
            var us = context.User.LastOrDefault();
            int newid = 0;
            if (us != null) newid = us.Id + 1;
            var newuser = new User
            {
                Id = newid,
                Login = login,
                Password = password
            };
            context.Add(newuser);
            context.SaveChanges();
            return true;
        }
    }
}
