﻿using System;
using System.Collections.Generic;

namespace Inf_System
{
    public partial class Notification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Txt { get; set; }
        public DateTime PrevDate { get; set; }
        public int Time { get; set; }
        public int? Uid { get; set; }

        public User U { get; set; }
    }
}
