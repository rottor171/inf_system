﻿using System;
using System.Collections.Generic;

namespace Inf_System
{
    public partial class User
    {
        public User()
        {
            Notification = new HashSet<Notification>();
            USLink = new HashSet<USLink>();
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public ICollection<Notification> Notification { get; set; }
        public ICollection<USLink> USLink { get; set; }

    }
}
