﻿using System;
using System.Collections.Generic;

namespace Inf_System
{
    public partial class USLink
    {
        public int Id { get; set; }
        public int? Uid { get; set; }
        public int? Sid { get; set; }
        public int Time { get; set; }
        public DateTime LastDate { get; set; }
        public bool? Seen { get; set; }

        public WebSite S { get; set; }
        public User U { get; set; }
    }
}
