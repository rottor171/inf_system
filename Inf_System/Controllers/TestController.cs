﻿using Inf_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Inf_System.Controllers
{
    public class TestController : Controller
    {
        public struct Person
        {
            public string Name;
            public string Surname;
            public int Age;
        }
        // GET: Test
        public ActionResult NewTest()
        {
            return View();
        }

        // POST: Test
        [HttpPost]
        [AllowAnonymous]
        public ActionResult NewTest(NewTestViewModel model) //async Task<ActionResult>
        {
            if (ModelState.IsValid)
            {
                if (model.Field != 0)
                {
                    Person p1 = new Person() { Name = "Ivan", Surname = "Brotan1", Age = 26 };
                    Person p2 = new Person() { Name = "Bodya", Surname = "Brotan2", Age = 22 };
                    Person p3 = new Person() { Name = "Boris", Surname = "Brotan3", Age = 25 };
                    Person[] li = new Person[3] { p1, p2, p3 };
                    var json = JsonConvert.SerializeObject(li);
                    return Json(json);
                }
                else return Content("foo");
            }
            return View(model);
        }
        // GET: Test
        public ActionResult Test()
        {
            return View();
        }

        // POST: Test
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Test(TestViewModel model) //async Task<ActionResult>
        {
            if (ModelState.IsValid)
            {
                string f1 = model.Field1;
                string f2 = model.Field2;
                int f3 = model.Field3;

                //AddErrors(result);
                string fullHtmlCode = "";//"<!DOCTYPE html><html><head>";
                                         //fullHtmlCode += "<title>Главная страница</title>";
                                         //fullHtmlCode += "<meta charset=utf-8 />";
                                         //fullHtmlCode += "</head> <body>";
                fullHtmlCode += f1 + f2;
                //fullHtmlCode += "</body></html>";
                //HttpContext.Response.Write(fullHtmlCode);

            }
            //return RedirectToAction("Index", "Home"); RedirectToAction("Register", "Account");
            return View(model);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
        